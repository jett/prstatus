package com.aurea.prstatus;

import com.aurea.prstatus.integration.ReviewRequestService;
import com.aurea.prstatus.integration.reviewboard.ReviewBoardReviewRequestServiceImpl;
import com.aurea.prstatus.integration.stash.StashReviewRequestServiceImpl;
import com.aurea.prstatus.module.BlahRepository;
import com.aurea.prstatus.report.EmailReportGenerator;

import java.util.Set;

public class ReviewRequestSummarizer {

    public static String DF_STASH_BASEURL = "https://scm.devfactory.com/stash/rest/api/1.0/";
    public static String DF_STASH_USER = "service.aurea.stash";
    public static String DF_STASH_PWD = "Nq&(`XK<58yC=x4#";

    public static String AUREA_STASH_BASEURL = "http://stash.aurea.local/rest/api/1.0/";
    public static String AUREA_STASH_USER = "review.service";
    public static String AUREA_STASH_PWD = "wh38ifkPoU";

    public static void main(String[] args) {

        try {
            if(args.length < 1) {
                System.out.println("please specify project to check");
            } else {

                BlahRepository repository = BlahRepository.getInstance();

                // DF Stash
                ReviewRequestService dfStash =
                        new StashReviewRequestServiceImpl(DF_STASH_BASEURL, DF_STASH_USER, DF_STASH_PWD);

//                Set<PRReviewRequest> dfRequests = dfStash.getOpenRequestsForProject("TDOS"); //.getAllOpenRequests();
                Set<ReviewRequest> dfRequests = dfStash.getAllOpenRequests();

                ReviewRequestService aureaStash =
                        new StashReviewRequestServiceImpl(AUREA_STASH_BASEURL, AUREA_STASH_USER, AUREA_STASH_PWD);

                Set<ReviewRequest> aureaRequests = aureaStash.getAllOpenRequests();

                // ReviewBoard
                ReviewRequestService reviewBoard =
                        new ReviewBoardReviewRequestServiceImpl("http://review.aurea.local/api/",
                                "review.service",
                                "wh38ifkPoU");

                Set<ReviewRequest> reviewBoardRequests = reviewBoard.getAllOpenRequests();

                EmailReportGenerator emailReportGenerator = new EmailReportGenerator();
                emailReportGenerator.generateAggregatedReport(dfRequests, reviewBoardRequests, aureaRequests);
//                emailReportGenerator.emailOutstandingPerReview("OSCC");
            }
        } catch(Exception e) {
            e.printStackTrace();
            System.out.println("An unexpected exception has occurred");
        }
    }
}
