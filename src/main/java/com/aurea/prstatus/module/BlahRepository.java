package com.aurea.prstatus.module;


public class BlahRepository {

    private static BlahRepository instance = null;

    protected BlahRepository() {
    }

    public static BlahRepository getInstance() {
        if(instance == null) {
            instance = new BlahRepository();
        }
        return instance;
    }
}
