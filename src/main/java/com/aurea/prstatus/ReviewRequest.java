package com.aurea.prstatus;

import java.util.Date;

public class ReviewRequest {

    public String vcs;
    public String project;
    public String repository;
    public Long prId;
    public String title;
    public Date createDate;
    public Date updateDate;
    public String author;
    public Reviewer reviewer;
    public String url;

    public ReviewRequest(String vcs,
                         String project,
                         String repository,
                         Long prId,
                         String title,
                         Date createDate,
                         Date updateDate,
                         String author,
                         String reviewer,
                         String reviewerEmail,
                         String url) {
        this.vcs = vcs;
        this.project = project;
        this.repository = repository;
        this.prId = prId;
        this.title = title;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.author = author;
        this.reviewer = new Reviewer(reviewer, reviewerEmail);
        this.url = url;
    }

    public double getDaysElapsed() {

        double diff = new Date().getTime() - createDate.getTime();
        diff = diff / (24 * 60 * 60 * 1000);

        return diff;
    }

}
