package com.aurea.prstatus.report;

import com.aurea.prstatus.ReviewRequest;
import com.aurea.prstatus.Reviewer;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.stream.Collectors.groupingBy;

public class Group {

    public static Map<Reviewer, List<ReviewRequest>> byReviewer(Set<ReviewRequest> reviewRequests) {

        Map<Reviewer, List<ReviewRequest>> requestsByReviewer = reviewRequests.stream()
                .collect(groupingBy(p -> p.reviewer));

        return requestsByReviewer;

    }

}
