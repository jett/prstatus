package com.aurea.prstatus.report;

import com.aurea.prstatus.ReviewRequest;
import com.aurea.prstatus.Reviewer;
import com.aurea.prstatus.email.EmailService;
import com.aurea.prstatus.integration.stash.StashRepository;
import com.aurea.prstatus.integration.stash.StashReviewRequestServiceImpl;
import com.aurea.prstatus.integration.stash.model.Repository;
import com.google.gdata.data.books.Review;

import java.text.SimpleDateFormat;
import java.util.*;

public class EmailReportGenerator {

    private StashRepository stashRepository;

    // todo: move this
    public static String DF_STASH_BASEURL = "https://scm.devfactory.com/stash/rest/api/1.0/";
    public static String DF_STASH_USER = "service.aurea.stash";
    public static String DF_STASH_PWD = "Nq&(`XK<58yC=x4#";

    public EmailReportGenerator() {
        stashRepository = new StashRepository(DF_STASH_BASEURL, DF_STASH_USER, DF_STASH_PWD);
    }

    /**
     * Emails a list of outstanding review items for all reviewers with outstanding review requests for the projectkey
     * specified
     *
     * @param project - project key of the project to send emails for
     * @throws Exception
     */
    public void emailOutstandingPerReview(String project) throws Exception {

        StashReviewRequestServiceImpl reviewRequestService =
                new StashReviewRequestServiceImpl(DF_STASH_BASEURL, DF_STASH_USER, DF_STASH_PWD);

        List<Repository> repositories = stashRepository.getProjectRepositories(project);

        for(Repository repository : repositories) {

            System.out.println("processing : " + repository.name);

            Set<ReviewRequest> prs = reviewRequestService.getOpenRequestsForRepository(project, repository.name);
            Map<Reviewer, List<ReviewRequest>> reportByPerson = Group.byReviewer(prs);

            for (Map.Entry<Reviewer, List<ReviewRequest>> entry : reportByPerson.entrySet())
            {

                Reviewer reviewer = entry.getKey();

                String emailBody = "The following pull requests are waiting for your review.<br/><br/>";
                emailBody += createEmailBodyForOutstandingReviewerReviews(reportByPerson.get(reviewer));

                // = String.format("<div style=\"font-weight:bold;\"><u>%s</u></div>", repository.name);

                String to = "jett.gamboa@aurea.com";    // todo : this should be the reviewer
                String cc = ""; // todo: this should be the SEM
                String subject = "ACTION REQUIRED: Review PRs for "
                        + project + " - " + repository.name
                        + " (" + reviewer.email + ")";  // todo: for demo only

                EmailService emailService = new EmailService();

                emailService.sendHTMLEmail(to, cc, subject, emailBody);

                // rest so we don't get flagged
                Thread.sleep(3000);

            }
        }
    }

    /**
     * Create the HTML message containing summary of outstanding review requests
     * @param reviewRequests - a list of PRReviewRequests to create a report for
     * @return
     */
    private String createEmailBodyForOutstandingReviewerReviews(List<ReviewRequest> reviewRequests) {

        StringBuilder sb = new StringBuilder();

        SimpleDateFormat ymdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        Comparator<ReviewRequest> comparator = Comparator.comparing(prReviewRequest -> prReviewRequest.createDate);

        sb.append("<table>");
        sb.append("<thead style=\"font-weight: bold;\"><td>Author</td><td>PR id</td><td>Title</td><td>PR Create Date (UTC)</td><td>Days Elapsed</td><td></td></thead>");
        sb.append("<tbody>");

        reviewRequests.stream()
                .sorted(comparator)
                .forEach(e -> {
                            System.out.format("%s\t%s\t%s\t%s\t%2.2f\t%s\n", e.reviewer.name,
                                    e.prId,
                                    e.title.substring(0, (e.title.length() > 20 ? 20 : e.title.length())),
                                    ymdFormat.format(e.createDate),
                                    e.getDaysElapsed(),
                                    e.url);

                            sb.append(String.format("<tr><td width=\"200px\">%-20s</td><td width=\"50px\">%-5s</td><td width=\"200px\">%s</td><td width=\"150px\">%s</td><td width=\"100px\">%2.2f</td><td><a href=\"%s\">review</a></td></tr>",
                                    e.author,
                                    e.prId,
                                    //e.title.substring(0, (e.title.length() > 20 ? 20 : e.title.length())),
                                    e.title,
                                    ymdFormat.format(e.createDate),
                                    e.getDaysElapsed(),
                                    e.url));

                        }
                );

        sb.append("</tbody>");
        sb.append("</table>");

        return sb.toString();
    }


    private class ReviewRequestAverage {

        Reviewer reviewer;
        Long average;
        List<ReviewRequest> reviewRequests;

        public ReviewRequestAverage(Reviewer reviewer, Long average, List<ReviewRequest> reviewRequests) {
            this.reviewer = reviewer;
            this.average = average;
            this.reviewRequests = reviewRequests;
        }

        public Long getCount() {
            return Integer.valueOf(reviewRequests.size()).longValue();
        }

    }

    private String generateSummaryForEmail(Set<ReviewRequest> reviewRequests) {

        Map<Reviewer, List<ReviewRequest>> reportByPerson = Group.byReviewer(reviewRequests);

        StringBuilder sb = new StringBuilder();

        sb.append("<table>");
        sb.append("<thead style=\"font-weight: bold; color: rgb(255,255,255); background-color: rgb(0,0,0);\">" +
                "<td>Reviewer</td><td>Number of PRs (>24h)</td><td>Days Elapsed (avg)</td></thead>");
        sb.append("<tbody>");

        Map<Reviewer, List<ReviewRequest>> sortedReportByPerson = new TreeMap<>(reportByPerson);
        List<ReviewRequestAverage> reviewRequestAverages = new ArrayList<>();

        long total = 0;

        // get the average
        for (Map.Entry<Reviewer, List<ReviewRequest>> entry : sortedReportByPerson.entrySet()) {

            Reviewer reviewer = entry.getKey();

            // get the average # of days elapsed per reviewer
            double average = entry.getValue().stream().parallel()
                    .filter(u -> (u.getDaysElapsed() > 0 ))
                    .mapToDouble(u -> u.getDaysElapsed())
                    .average()
                    .getAsDouble();

            reviewRequestAverages.add(new ReviewRequestAverage(reviewer, Math.round(average), entry.getValue()));

//            sb.append(String.format("<tr><td width=\"200px\">%-20s</td><td width=\"50px\">%-5s</td><td width=\"50px\">%-2.2f</td></tr>",
//                    reviewer.name,
//                    entry.getValue().size(),
//                    average));

            total += entry.getValue().size();
        }


        // sort by count and then by average
        Comparator<ReviewRequestAverage> comparator = Comparator.comparing(prReviewRequest -> prReviewRequest.reviewRequests.size());
        comparator = comparator.reversed().thenComparing(Comparator.comparing(prReviewRequest -> prReviewRequest.average)).reversed();

        Comparator<ReviewRequestAverage> byTotalThenAverageComparator = new Comparator<ReviewRequestAverage>() {
            @Override
            public int compare(ReviewRequestAverage o1, ReviewRequestAverage o2) {

                int byCount = o2.getCount().compareTo(o1.getCount());

                if(byCount==0) {
                    return o2.average.compareTo(o1.average);
                } else {
                    return byCount;
                }
            }
        };

        reviewRequestAverages.stream()
                .sorted(byTotalThenAverageComparator)
                .forEach(e -> {

                    sb.append(String.format("<tr><td width=\"200px\">%-20s</td><td width=\"50px\">%-5s</td><td width=\"50px\">%-5s</td></tr>",
                            e.reviewer.name,
                            e.reviewRequests.size(),
                            e.average));

                });


        sb.append(String.format("<tr><td></td><td>%s</td><td></td></tr>", total));

        sb.append("</tbody>");
        sb.append("</table>");

        return sb.toString();

    }


    public void generateAggregatedReport(Set<ReviewRequest> dfReviewRequests,
                                         Set<ReviewRequest> reviewBoardReviewRequests,
                                         Set<ReviewRequest> aureaReviewRequests) throws Exception {

        Calendar now  = Calendar.getInstance();
        System.out.println("generating email");

        StringBuilder sb = new StringBuilder();

        // generate Google sheet
        // todo: run email and sheet generate as separate threads and then send the email after content has been
        // generated
        GoogleSheetReportGenerator reportGenerator = new GoogleSheetReportGenerator();
        String url = reportGenerator.generateReportForAllProjects(dfReviewRequests, "DFStash");
        url = reportGenerator.generateReportForAllProjects(reviewBoardReviewRequests, "ReviewBoard");
        url = reportGenerator.generateReportForAllProjects(aureaReviewRequests, "AureaStash");
        // end todo

        sb.append(String.format("Hello All,<br/><br/>Please find the code review turnaround report for week %s <a href='%s'>here</a>." +
                "<br/><br/>Overview:<br/><br/>", now.get(Calendar.WEEK_OF_YEAR)-1, url));

        sb.append("<b>DevFactory Stash</b><br/>");
        sb.append(generateSummaryForEmail(dfReviewRequests));
        sb.append("<b>ReviewBoard</b><br/>");
        sb.append(generateSummaryForEmail(reviewBoardReviewRequests));
        sb.append("<b>Aurea Stash</b><br/>");
        sb.append(generateSummaryForEmail(aureaReviewRequests));

        sb.append("<br/>Thanks!<br/>");

        String emailBody = sb.toString();

//        String to = "jett.gamboa@aurea.com,bartosz.rybusinski@aurea.com,jozsef.czapovics@aurea.com";    // todo : this should be the SEM distribution list here
        String to = "jett.gamboa@aurea.com";
//        String to = "software-engineering-managers@aurea.com, trilogyengvps@aurea.com, marc.breslow@devfactory.com";
        String cc = ""; // todo: this should be the SEM

//        String subject = "Weekly Code Review Turnaround Report - Week " + (now.get(Calendar.WEEK_OF_YEAR)-1);
        String subject = "Weekly Code Review Turnaround Report - Week " + (now.get(Calendar.WEEK_OF_YEAR)-1);

        EmailService emailService = new EmailService();
        emailService.sendHTMLEmail(to, cc, subject, emailBody);
    }

}
