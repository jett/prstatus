package com.aurea.prstatus.report;

import com.aurea.prstatus.ReviewRequest;
import com.aurea.prstatus.integration.ReviewRequestService;
import com.aurea.prstatus.integration.stash.StashRepository;
import com.aurea.prstatus.integration.stash.StashReviewRequestServiceImpl;
import com.aurea.prstatus.misc.ProgressBar;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.ParentReference;
import com.google.api.services.drive.model.Permission;
import com.google.gdata.client.spreadsheet.SpreadsheetQuery;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.Link;
import com.google.gdata.data.batch.BatchOperationType;
import com.google.gdata.data.spreadsheet.*;
import com.google.gdata.model.batch.BatchUtils;
import com.google.gdata.util.ServiceException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;


// http://stackoverflow.com/questions/30483601/create-spreadsheet-using-google-spreadsheet-api-in-google-drive-in-java
// https://templth.wordpress.com/2014/11/19/interacting-with-google-spreadsheet-with-java/

// experiment here:  https://developers.google.com/drive/v2/reference/files/list#try-it

public class GoogleSheetReportGenerator {

    private StashRepository stashRepository;

    // todo: move this
    public static String DF_STASH_BASEURL = "https://scm.devfactory.com/stash/rest/api/1.0/";
    public static String DF_STASH_USER = "service.aurea.stash";
    public static String DF_STASH_PWD = "Nq&(`XK<58yC=x4#";

    // my variables
    private static final String SPREADSHEET_FEED_URI = "https://spreadsheets.google.com/feeds/spreadsheets/private/full";
    private SpreadsheetService spreadsheetService;
    private Drive drive;
    private URL cellFeedUrl;
    // end my variables

    public final static String MIMETYPE_SHEET = "application/vnd.google-apps.spreadsheet";

    private static final String APPLICATION_NAME =
            "PR Status Checker";

    private static final java.io.File DATA_STORE_DIR = new java.io.File(
            System.getProperty("user.home"), ".credentials/prstatus");

    private static FileDataStoreFactory DATA_STORE_FACTORY;

    private static final JsonFactory JSON_FACTORY =
            JacksonFactory.getDefaultInstance();

    private static HttpTransport HTTP_TRANSPORT;

    private static final List<String> SCOPES =
            Arrays.asList("https://www.googleapis.com/auth/drive",
                    "https://spreadsheets.google.com/feeds/spreadsheets/private/full");

    static {
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }



    private Credential authorize() throws IOException {
        // Load client secrets.
        InputStream in =
                GoogleSheetReportGenerator.class.getResourceAsStream("/client_secret.json");
        GoogleClientSecrets clientSecrets =
                GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow =
                new GoogleAuthorizationCodeFlow.Builder(
                        HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                        .setDataStoreFactory(DATA_STORE_FACTORY)
                        .setAccessType("offline")
                        .build();
        Credential credential = new AuthorizationCodeInstalledApp(
                flow, new LocalServerReceiver()).authorize("user");
        System.out.println("Credentials saved to " + DATA_STORE_DIR.getAbsolutePath());
        return credential;
    }


    public GoogleSheetReportGenerator() throws Exception {

        Credential credential = authorize();

        stashRepository = new StashRepository(DF_STASH_BASEURL, DF_STASH_USER, DF_STASH_PWD);

        spreadsheetService = new SpreadsheetService("data");
        spreadsheetService.setOAuth2Credentials(credential);

        drive = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();

    }


    private Boolean fileExists(String filename) throws IOException {

        final String queryParameter = String.format("title='%s' AND trashed=false", filename);
        FileList results = drive.files().list().setQ(queryParameter).execute();

        return results.getItems().size() > 0;
    }


    private File getFile(String filename) throws IOException {

        final String queryParameter = String.format("title='%s' AND trashed=false", filename);
        FileList results = drive.files().list().setQ(queryParameter).execute();

        if(results.getItems().size() == 1) {
            return results.getItems().get(0);
        } else {
            return null;
        }
    }


    private String copyFile(String originalFile, String copy) throws IOException {

        File newFile;

        // file does not exist copy the template into this new file
        if(!fileExists(copy)) {
            File oldFile = getFile(originalFile);

            newFile = new File();
            newFile.setTitle(copy);

            drive.files().copy(oldFile.getId(), newFile).execute();
        }

        Permission filePermission = new Permission();
        filePermission.setType("anyone");
        filePermission.setRole("writer");
        filePermission.setValue("");
        filePermission.setWithLink(true);

        newFile = getFile(copy);
        Permission np = drive.permissions().insert(newFile.getId(), filePermission).execute();

        return newFile.getAlternateLink();
    }


    private SpreadsheetEntry getWorksheet(String worksheetName) throws IOException, ServiceException {

        URL spreadSheetFeedUrl = new URL(SPREADSHEET_FEED_URI);

        // retrieve a reference to the sheet and return it
        SpreadsheetQuery spreadsheetQuery = new SpreadsheetQuery(
                spreadSheetFeedUrl);
        spreadsheetQuery.setTitleQuery(worksheetName);
        spreadsheetQuery.setTitleExact(true);
        SpreadsheetFeed spreadsheet = this.spreadsheetService.getFeed(spreadsheetQuery,
                SpreadsheetFeed.class);

        if(spreadsheet.getEntries().size() == 1) {
            return spreadsheet.getEntries().get(0);
        } else {
            return null;
        }
    }


    private SpreadsheetEntry createWorksheet(String worksheetName) throws IOException, ServiceException, InterruptedException {
        File newWorksheet = new File();
        newWorksheet.setTitle(worksheetName);
        newWorksheet.setMimeType(MIMETYPE_SHEET);
        newWorksheet.setParents(Arrays.asList(new ParentReference().setId("root")));

        File worksheet = drive.files().insert(newWorksheet).execute();

        URL spreadSheetFeedUrl = new URL(SPREADSHEET_FEED_URI);

        // retrieve a reference to the sheet and return it
        SpreadsheetQuery spreadsheetQuery = new SpreadsheetQuery(
                spreadSheetFeedUrl);
        spreadsheetQuery.setTitleQuery(worksheetName);
        spreadsheetQuery.setTitleExact(true);
        SpreadsheetFeed spreadsheet = this.spreadsheetService.getFeed(spreadsheetQuery,
                SpreadsheetFeed.class);

        // guaranteed to return because we create the file here
        return spreadsheet.getEntries().get(0);
    }


    public void setCell(WorksheetEntry worksheetEntry, int row, int col, String formulaOrValue)
            throws IOException, ServiceException {

        cellFeedUrl = worksheetEntry.getCellFeedUrl();

        CellEntry newEntry = new CellEntry(row, col, formulaOrValue);
        spreadsheetService.insert(cellFeedUrl, newEntry);
    }

    public CellEntry setCell(CellFeed batchRequest, WorksheetEntry worksheetEntry, int row, int col, String formulaOrValue)
            throws IOException, ServiceException {

        String batchId = "R" + row + "C" + col; // experimental

        cellFeedUrl = worksheetEntry.getCellFeedUrl();

        CellEntry newEntry = new CellEntry(row, col, formulaOrValue);
        newEntry.setId(String.format("%s/%s", cellFeedUrl.toString(), batchId));
        //spreadsheetService.insert(cellFeedUrl, newEntry);

        // add attributes for batch
        BatchUtils.setBatchId(newEntry, batchId);
        BatchUtils.setBatchOperationType(newEntry, BatchOperationType.UPDATE);

        // add to our batch request
        batchRequest.getEntries().add(newEntry);

        return newEntry;

    }

    public String generateReportForAllProjects(Set<ReviewRequest> prs, String sheetName) throws Exception {
        return generateReportForAllProjects(prs, sheetName, false);
    }


    private WorksheetEntry getSheetWithName(SpreadsheetEntry spreadsheetEntry, String sheetName) throws IOException, ServiceException {

        for(WorksheetEntry worksheetEntry : spreadsheetEntry.getWorksheets()) {
            if(worksheetEntry.getTitle().getPlainText().equals(sheetName)) {
                return worksheetEntry;
            }
        }

        return null;
    }


    public String generateReportForAllProjects(Set<ReviewRequest> prs, String sheetName, Boolean dev) throws Exception {

        SimpleDateFormat ymdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        Calendar now  = Calendar.getInstance();

        String worksheetName = String.format("Outstanding Review Requests %s Week %s",
                now.getWeekYear(),
                (now.get(Calendar.WEEK_OF_YEAR)-1));

        // prepend TEST if this is called from a dev run
        if(dev) {
            worksheetName = "TEST ::: " + worksheetName;
        }

        // copy from the template
        String linkToFile = copyFile("Outstanding PRs Template", worksheetName);
        SpreadsheetEntry spreadsheet = getWorksheet(worksheetName);

        if(spreadsheet != null) {

            //WorksheetEntry worksheet = spreadsheet.getDefaultWorksheet();
            WorksheetEntry worksheet = getSheetWithName(spreadsheet, sheetName);    //todo: hardcoded

            int row = 2;

            Comparator<ReviewRequest> comparatorByProject = Comparator.comparing(prReviewRequest -> prReviewRequest.project);
            List<ReviewRequest> reviewRequests = new ArrayList<>(prs);

            reviewRequests.sort(comparatorByProject);

            CellFeed batchRequest = new CellFeed();
            List<CellEntry> cellEntries = new ArrayList<>();
            int batchCounter = 0;

            if(reviewRequests.size() > 0) {

                ProgressBar progressBar = new ProgressBar(reviewRequests.size(), "Writing entries to Google Sheet - " + sheetName);
                progressBar.setVal(row);

                for (ReviewRequest pr : reviewRequests) {

                    // todo: optimization using batch requests
                    cellEntries.add(setCell(batchRequest, worksheet, row, 1, pr.project));
                    cellEntries.add(setCell(batchRequest, worksheet, row, 2, pr.repository));
                    cellEntries.add(setCell(batchRequest, worksheet, row, 3, pr.reviewer.name));
                    cellEntries.add(setCell(batchRequest, worksheet, row, 4, pr.author));
                    cellEntries.add(setCell(batchRequest, worksheet, row, 5, pr.prId.toString()));
                    cellEntries.add(setCell(batchRequest, worksheet, row, 6, pr.title));
                    cellEntries.add(setCell(batchRequest, worksheet, row, 7, ymdFormat.format(pr.createDate)));
                    cellEntries.add(setCell(batchRequest, worksheet, row, 8, String.valueOf(pr.getDaysElapsed())));
                    cellEntries.add(setCell(batchRequest, worksheet, row, 9, pr.url));

                    row++;

                    batchCounter += 9;

                    // send out in batches of 900 (API limit is 1000)
                    if (batchCounter >= 180) {

                        CellFeed cellFeed = spreadsheetService.getFeed(worksheet.getCellFeedUrl(), CellFeed.class);
                        Link batchLink = cellFeed.getLink(Link.Rel.FEED_BATCH, Link.Type.ATOM);

                        spreadsheetService.setHeader("If-Match", "*");
                        CellFeed batchResponse = spreadsheetService.batch(new URL(batchLink.getHref()), batchRequest);
                        spreadsheetService.setHeader("If-Match", null);

                        batchRequest.getEntries().clear();
                        batchCounter = 0;
                    }

                    progressBar.setVal(row);
                }

                batchRequest.getEntries().addAll(cellEntries);
                //
                CellFeed cellFeed = spreadsheetService.getFeed(worksheet.getCellFeedUrl(), CellFeed.class);
                Link batchLink = cellFeed.getLink(Link.Rel.FEED_BATCH, Link.Type.ATOM);

                spreadsheetService.setHeader("If-Match", "*");
                CellFeed batchResponse = spreadsheetService.batch(new URL(batchLink.getHref()), batchRequest);
                spreadsheetService.setHeader("If-Match", null);

                progressBar.finish();
            }
        }

        return linkToFile;

    }


    public class GSheetReportGenerationRunnable implements Runnable {
        public void run() {
            System.out.println("Hello from a thread!");
            try {
                Thread.sleep(5000);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Runnable getReportRunner() {

        return new GSheetReportGenerationRunnable();

    }


    public static void main(String[] args) throws Exception {

        String DF_STASH_BASEURL = "https://scm.devfactory.com/stash/rest/api/1.0/";
        String DF_STASH_USER = "service.aurea.stash";
        String DF_STASH_PWD = "Nq&(`XK<58yC=x4#";

        ReviewRequestService dfStash =
                new StashReviewRequestServiceImpl(DF_STASH_BASEURL, DF_STASH_USER, DF_STASH_PWD);

        // get all requests
        Set<ReviewRequest> dfRequests = dfStash.getOpenRequestsForProject("OSCC");

        // generate sheet
        GoogleSheetReportGenerator g = new GoogleSheetReportGenerator();
        System.out.println(g.generateReportForAllProjects(dfRequests, "DFStash"));

    }
}
