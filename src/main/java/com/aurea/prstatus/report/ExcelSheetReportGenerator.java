package com.aurea.prstatus.report;

public class ExcelSheetReportGenerator {

//    private StashRepository stashRepository;
//
//    // todo: move this
//    public static String DF_STASH_BASEURL = "https://scm.devfactory.com/stash/rest/api/1.0/";
//    public static String DF_STASH_USER = "service.aurea.stash";
//    public static String DF_STASH_PWD = "Nq&(`XK<58yC=x4#";
//
//    public ExcelSheetReportGenerator() {
//        stashRepository = new StashRepository(DF_STASH_BASEURL, DF_STASH_USER, DF_STASH_PWD);
//    }
//
//    public void generateReportByRepository(String project) throws Exception {
//
//        StashReviewRequestServiceImpl reviewRequestService = new StashReviewRequestServiceImpl();
//
//        SimpleDateFormat ymdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//
//        List<Repository> repositories = stashRepository.getProjectRepositories(project);
//
//        for(Repository repository : repositories) {
//
//            System.out.println("processing : " + repository.name);
//
//            Set<PRReviewRequest> prs = reviewRequestService.getOpenRequestsForRepository(project, repository.name);
//            //Map<Reviewer, List<PRReviewRequest>> reportByPerson = Group.byReviewer(prs);
//
//            for (PRReviewRequest pr : prs)
//            {
//
//                System.out.format("%s\t%s\t%s\t%s\t%2.2f\t%s\n", pr.reviewer.name,
//                        pr.prId,
//                        pr.title,
//                        ymdFormat.format(pr.createDate),
//                        pr.getDaysElapsed(),
//                        pr.url);
//            }
//        }
//    }
//
//    public void generateReportForAllProjects() throws Exception {
//
//        StashReviewRequestServiceImpl reviewRequestService = new StashReviewRequestServiceImpl();
//
//        SimpleDateFormat ymdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//
//        //Blank workbook
//        XSSFWorkbook workbook = new XSSFWorkbook();
//
//        //Create a blank sheet
//        XSSFSheet sheet = workbook.createSheet("Open PR Requests");
//        int rownum = 0;
//
//        List<Project> allProjects = stashRepository.getProjects();
//
//        Row headerRow = sheet.createRow(rownum++);
//        Cell headerCell = headerRow.createCell(0);
//        headerCell.setCellValue("Project");
//        headerCell = headerRow.createCell(1);
//        headerCell.setCellValue("Repository");
//        headerCell = headerRow.createCell(2);
//        headerCell.setCellValue("Reviewer");
//        headerCell = headerRow.createCell(3);
//        headerCell.setCellValue("Author");
//        headerCell = headerRow.createCell(4);
//        headerCell.setCellValue("PR Id");
//        headerCell = headerRow.createCell(5);
//        headerCell.setCellValue("Title");
//        headerCell = headerRow.createCell(6);
//        headerCell.setCellValue("PR Date Created");
//        headerCell = headerRow.createCell(7);
//        headerCell.setCellValue("Days Elapsed");
//        headerCell = headerRow.createCell(8);
//        headerCell.setCellValue("URL");
//
//        // debugging
////        Project tst = new Project();
////        tst.name = "OSCC";
////        tst.key = "OSCC";
//
////        allProjects.clear();;
////        allProjects.add(tst);
//        // end debugging
//
//        for(Project project : allProjects) {
//
//            System.out.println("processing : " + project.name);
//
//            List<Repository> repositories = stashRepository.getProjectRepositories(project.key);
//
//            for(Repository repository : repositories) {
//
//                System.out.println("processing : " + repository.name);
//
//                Set<PRReviewRequest> prs = reviewRequestService.getOpenRequestsForRepository(project.key, repository.name);
//
//                for (PRReviewRequest pr : prs)
//                {
//
//                    Row row = sheet.createRow(rownum++);
//                    Cell cell = row.createCell(0);
//                    cell.setCellValue(pr.project);
//                    cell = row.createCell(1);
//                    cell.setCellValue(pr.repository);
//                    cell = row.createCell(2);
//                    cell.setCellValue(pr.reviewer.name);
//                    cell = row.createCell(3);
//                    cell.setCellValue(pr.author);
//                    cell = row.createCell(4);
//                    cell.setCellValue(pr.prId);
//                    cell = row.createCell(5);
//                    cell.setCellValue(pr.title);
//                    cell = row.createCell(6);
//                    cell.setCellValue(ymdFormat.format(pr.createDate));
//                    cell = row.createCell(7);
//                    cell.setCellValue(pr.getDaysElapsed());
//                    cell = row.createCell(8);
//                    cell.setCellValue(pr.url);
//
//                    System.out.format("%s\t%s\t%s\t%s\t%2.2f\t%s\n", pr.reviewer.name,
//                            pr.prId,
//                            pr.title,
//                            ymdFormat.format(pr.createDate),
//                            pr.getDaysElapsed(),
//                            pr.url);
//                }
//            }
//        }
//
//        try
//        {
//            //Write the workbook in file system
//            FileOutputStream out = new FileOutputStream(new File("Open_Review_Requests.xlsx"));
//            workbook.write(out);
//            out.close();
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//
//    }

}
