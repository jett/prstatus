package com.aurea.prstatus.integration.stash.model;

import java.util.Date;
import java.util.List;

/**
 * This class represents the resource returned by the pull request activities API:
 * https://developer.atlassian.com/static/rest/stash/3.11.3/stash-rest.html#idp137840
 */
public class Activities {

    public Long id;
    public Date createdDate;
    User user;
    String action;

}
