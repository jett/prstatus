package com.aurea.prstatus.integration.stash;

import com.aurea.prstatus.integration.ServiceBuilder;
import com.aurea.prstatus.integration.stash.model.Project;
import com.aurea.prstatus.integration.stash.model.PullRequest;
import com.aurea.prstatus.integration.stash.model.Repository;
import com.google.gson.*;
import retrofit2.Call;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;

public class StashRepository {

    StashService stashService;

    public StashRepository(String baseUrl, String userid, String password) {
        this.stashService = ServiceBuilder.createService(StashService.class, baseUrl, userid, password);
    }

    public List<Project> getProjects() throws IOException {

        Long offset = 0L;
        Gson gson = new Gson();
        List<Project> projects = new ArrayList<>();
        Boolean isLastPage = false;

        while(!isLastPage) {

            Call<StashResponse> call = stashService.getProjects(offset);
            StashResponse response = call.execute().body();

            // if no values, no need to iterate
            if(response == null) {
                break;
            }

            if(response.values.size() > 0) {
                Iterator<Map> iter = response.values.iterator();

                while (iter.hasNext()) {
                    Map repoMap = iter.next();

                    JsonElement jsonElement = gson.toJsonTree(repoMap);
                    Project project = gson.fromJson(jsonElement, Project.class);

                    projects.add(project);
                }
            }

            isLastPage = response.isLastPage;
            offset = response.nextPageStart;

        }
        return projects;
    }


    public List<Repository> getProjectRepositories(String project) throws IOException {

        Long offset = 0L;
        Gson gson = new Gson();
        List<Repository> repositories = new ArrayList<>();
        Boolean isLastPage = false;

        while(!isLastPage) {

            Call<StashResponse> call = stashService.getProjectRepos(project, offset);
            StashResponse response = call.execute().body();

            // if no values, no need to iterate
            if(response == null) {
                break;
            }

            if(response.values.size() > 0) {
                Iterator<Map> iter = response.values.iterator();

                while (iter.hasNext()) {
                    Map repoMap = iter.next();

                    JsonElement jsonElement = gson.toJsonTree(repoMap);
                    Repository repo = gson.fromJson(jsonElement, Repository.class);

                    repositories.add(repo);
                }
            }

            isLastPage = response.isLastPage;
            offset = response.nextPageStart;

        }

        return repositories;
    }

    public List<PullRequest> getPullRequests(String project, String repository) throws IOException {

        // We deal with dates here so we must make a special deserializer for dates stored in epoc as Long
        GsonBuilder builder = new GsonBuilder();

        builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                return new Date(json.getAsJsonPrimitive().getAsLong());
            }
        });

        Gson gson = builder.create();

        Long offset = 0L;

        List<PullRequest> pullRequests = new ArrayList<>();
        Boolean isLastPage = false;

        while(!isLastPage) {

            Call<StashResponse> call = stashService.getPullRequests(project, repository, offset);
            StashResponse response = call.execute().body();

            // if no values, no need to iterate
            if(response == null) {
                break;
            }

            if(response.values.size() > 0) {
                Iterator<Map> iter = response.values.iterator();

                while (iter.hasNext()) {
                    Map repoMap = iter.next();

                    JsonElement jsonElement = gson.toJsonTree(repoMap);
                    PullRequest pr = gson.fromJson(jsonElement, PullRequest.class);

                    pullRequests.add(pr);
                }
            }

            isLastPage = response.isLastPage;
            offset = response.nextPageStart;

        }

        return pullRequests;
    }
}
