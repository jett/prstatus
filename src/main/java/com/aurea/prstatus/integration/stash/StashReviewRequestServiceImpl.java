package com.aurea.prstatus.integration.stash;

import com.aurea.prstatus.ReviewRequest;
import com.aurea.prstatus.integration.ReviewRequestService;
import com.aurea.prstatus.integration.stash.model.Project;
import com.aurea.prstatus.integration.stash.model.PullRequest;
import com.aurea.prstatus.integration.stash.model.Repository;
import com.aurea.prstatus.integration.stash.model.UserApproval;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

public class StashReviewRequestServiceImpl implements ReviewRequestService {

    private StashRepository stashRepository;

    public StashReviewRequestServiceImpl(String baseUrl, String user, String password) {
        stashRepository = new StashRepository(baseUrl, user, password);
    }

    @Override
    public Set<ReviewRequest> getAllOpenRequests() throws Exception {

        Set<ReviewRequest> allRequests = new HashSet<>();
        List<Project> allProjects = stashRepository.getProjects();

        for(Project project : allProjects) {

            System.out.println("project: " + project.name);

            Set<ReviewRequest> prs = getOpenRequestsForProject(project.key);
            allRequests.addAll(prs);

        }

        return allRequests;
    }

    @Override
    public Set<ReviewRequest> getOpenRequestsForProject(String project) throws Exception {

        List<Repository> repositories = stashRepository.getProjectRepositories(project);
        Set<ReviewRequest> reviewRequestSet = new HashSet<>();

        for(Repository repository : repositories) {

            System.out.println("\t-repository: " + repository.name);

            Set<ReviewRequest> prs = getOpenRequestsForRepository(project, repository.name);
            reviewRequestSet.addAll(prs);

        }

        return reviewRequestSet;
    }

    // todo: experimental, using functional interfaces
    public Set<ReviewRequest> retrieveAndProcessPerRepository(String project,
                                                                Function<Set<ReviewRequest>, Boolean> function) throws Exception {

        List<Repository> repositories = stashRepository.getProjectRepositories(project);
        Set<ReviewRequest> reviewRequestSet = new HashSet<>();

        for(Repository repository : repositories) {

            Set<ReviewRequest> prs = getOpenRequestsForRepository(project, repository.name);
            function.apply(prs);

            reviewRequestSet.addAll(prs);

        }

        return reviewRequestSet;
    }

    @Override
    public Set<ReviewRequest> getOpenRequestsForRepository(String project, String repository) throws Exception {

        Set<ReviewRequest> reviewRequestSet = new HashSet<>();

        for(PullRequest pr : stashRepository.getPullRequests(project, repository)) {

            for(UserApproval reviewer : pr.reviewers) {

                // add to our list if this reviewer has not acted on the PR yet
                if(!reviewer.approved) {
                    ReviewRequest reviewRequest = new ReviewRequest("DF Stash",
                            project,
                            repository,
                            pr.id,
                            pr.title,
                            pr.createdDate,
                            pr.updatedDate,
                            pr.author.user.displayName,
                            reviewer.user.displayName,
                            reviewer.user.emailAddress,
                            pr.links.self.get(0).href
                    );

                    reviewRequestSet.add(reviewRequest);
                } else {

                    // change requested by Marc
                    // if one of the reviewers approved, do not include in the report
                    reviewRequestSet.clear();
                    return reviewRequestSet;

                }
            }
        }
        return reviewRequestSet;
    }

}
