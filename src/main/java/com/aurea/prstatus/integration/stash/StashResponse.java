package com.aurea.prstatus.integration.stash;

import java.util.Collection;
import java.util.Map;

public class StashResponse {

    String size;
    String limit;
    Boolean isLastPage;

    Collection<Map> values;

    Long nextPageStart;

}
