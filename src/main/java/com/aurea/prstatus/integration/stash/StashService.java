package com.aurea.prstatus.integration.stash;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * This represents selected(relevant) APIs from the Stash REST API
 * (https://developer.atlassian.com/static/rest/stash/3.11.3/stash-rest.html)
 */
public interface StashService {

    @GET("projects")
    Call<StashResponse> getProjects(@Query("start") Long start);

    @GET("projects/{projectKey}/repos")
    Call<StashResponse> getProjectRepos(@Path("projectKey") String projectKey, @Query("start") Long start);

    @GET("projects/{projectKey}/repos/{repositorySlug}/pull-requests")
    Call<StashResponse> getPullRequests(@Path("projectKey") String projectKey,
                                       @Path("repositorySlug") String repository, @Query("start") Long start);

    @GET("projects/{projectKey}/repos/{repositorySlug}/pull-requests/{PRid}")
    Call<StashResponse> getPullRequestActivities(@Path("projectKey") String projectKey,
                                        @Path("repositorySlug") String repository,
                                                 @Path("PRid") String pullRequestId,
                                                 @Query("start") Long start);
}
