package com.aurea.prstatus.integration.stash.model;

import java.util.Date;
import java.util.List;

public class PullRequest {

    public Long id;
    public Long version;
    public String title;
    public Boolean closed;

    public String state;
    public Boolean open;
    public Date createdDate;
    public Date updatedDate;

    public UserApproval author;
    public List<UserApproval> reviewers;

    public Links links;
}
