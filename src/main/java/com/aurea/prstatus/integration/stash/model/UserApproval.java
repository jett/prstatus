package com.aurea.prstatus.integration.stash.model;

public class UserApproval {

    public User user;
    public String role;
    public Boolean approved;

}
