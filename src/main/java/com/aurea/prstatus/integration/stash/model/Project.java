package com.aurea.prstatus.integration.stash.model;

public class Project {

    public String key;
    public String name;
    public String description;
    Link link;

}
