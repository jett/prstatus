package com.aurea.prstatus.integration;

import okhttp3.*;
import org.apache.commons.codec.binary.Base64;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

import java.io.IOException;

public class ServiceBuilder {

    private static OkHttpClient httpClient;

    public static <S> S createService(Class<S> serviceClass, String baseURL) {
        return createService(serviceClass, baseURL, null, null);
    }

    public static <S> S createService(Class<S> serviceClass, String baseURL, final String username, final String password) {

        if (username != null && password != null) {
            String credentials = username + ":" + password;
            final String basic = "Basic " + new Base64().encodeToString(credentials.getBytes());

            httpClient = new OkHttpClient.Builder().addInterceptor(new Interceptor() {

                public Response intercept(Interceptor.Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Authorization", basic)
                            .header("Accept", "application/json")
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            }).build();

        } else {
            httpClient = new OkHttpClient();
        }

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(baseURL)
                        .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.client(httpClient).build();
        return retrofit.create(serviceClass);
    }
}
