package com.aurea.prstatus.integration;

import com.aurea.prstatus.ReviewRequest;

import java.util.Set;

public interface ReviewRequestService {

    Set<ReviewRequest> getAllOpenRequests() throws Exception ;
    Set<ReviewRequest> getOpenRequestsForProject(String project) throws Exception ;
    Set<ReviewRequest> getOpenRequestsForRepository(String project, String repository) throws Exception ;

}
