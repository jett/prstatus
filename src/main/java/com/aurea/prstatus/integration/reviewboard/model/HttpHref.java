package com.aurea.prstatus.integration.reviewboard.model;

/**
 * This represents the url for various HTTP references returned in the Links resource
 */
public class HttpHref {

    public String href;
    public String method;

    public String title;
}
