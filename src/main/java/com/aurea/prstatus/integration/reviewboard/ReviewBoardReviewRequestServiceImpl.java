package com.aurea.prstatus.integration.reviewboard;

import com.aurea.prstatus.ReviewRequest;
import com.aurea.prstatus.integration.ReviewRequestService;
import com.aurea.prstatus.integration.reviewboard.model.HttpHref;
import com.aurea.prstatus.integration.reviewboard.model.User;

import java.io.IOException;
import java.util.*;

public class ReviewBoardReviewRequestServiceImpl implements ReviewRequestService {

    ReviewBoardRepository reviewBoardRepository;
    Map<String, User> allUsers = new HashMap<>();

    public ReviewBoardReviewRequestServiceImpl(String baseUrl, String username, String password) throws IOException {

        reviewBoardRepository = new ReviewBoardRepository(baseUrl, username, password);

        List<User> users = reviewBoardRepository.getUsers();

        for(User user : users) {
            allUsers.put(user.username, user);
        }
    }


    @Override
    public Set<ReviewRequest> getAllOpenRequests() throws Exception {

        List<com.aurea.prstatus.integration.reviewboard.model.ReviewRequest> requests = reviewBoardRepository.getReviewRequests();
        Set<ReviewRequest> reviewRequestSet = new HashSet<>();

        for(com.aurea.prstatus.integration.reviewboard.model.ReviewRequest request : requests) {

            // iterate through the users the review request was sent to
            for(HttpHref ref : request.target_people) {

                User reviewer;

                // ref.title contains the userid of the user assigned to review
                if(allUsers.containsKey(ref.title)) {
                    reviewer = allUsers.get(ref.title);

                    // add to our list if this reviewer has not acted on the PR yet
                    if(!request.approved) {

                        User author = allUsers.get(request.links.submitter.title);

                        ReviewRequest reviewRequest = new ReviewRequest("ReviewBoard",
                                request.links.repository != null ? request.links.repository.title : "",
                                request.links.repository != null ? request.links.repository.title : "",
                                request.id,
                                request.summary,
                                request.time_added,
                                request.last_updated,
                                author.fullname,
                                reviewer.fullname,
                                reviewer.email != null ? reviewer.email : "",
                                request.absolute_url
                        );

                        reviewRequestSet.add(reviewRequest);
                    }
//                    else {
//
//                        // change requested by Marc
//                        // if one of the reviewers approved, do not include in the report
//                        reviewRequestSet.clear();
//                        return reviewRequestSet;
//
//                    }
                }
            }


        }

        return reviewRequestSet;

    }


    @Override
    public Set<ReviewRequest> getOpenRequestsForProject(String project) throws Exception {
        return null;
    }


    @Override
    public Set<ReviewRequest> getOpenRequestsForRepository(String project, String repository) throws Exception {
        return null;
    }


    public static void main(String[] args) throws IOException {

        ReviewBoardReviewRequestServiceImpl requestService =
            new ReviewBoardReviewRequestServiceImpl("http://review.aurea.local/api/",
                    "review.service",
                    "wh38ifkPoU");

        try {

            Set<ReviewRequest> reviewRequests = requestService.getAllOpenRequests();

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

}
