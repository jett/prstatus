package com.aurea.prstatus.integration.reviewboard.model;

/**
 * This class is a partial representation of the links resource found in various other resource
 * https://www.reviewboard.org/docs/manual/2.0/webapi/2.0/resources/review-request/#links
 */
public class Links {

    public HttpHref self;
    public HttpHref create;
    public HttpHref next;
    public HttpHref submitter;
    public HttpHref repository;
}
