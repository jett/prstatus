package com.aurea.prstatus.integration.reviewboard.model;

/**
 * This class represents a partial representation of the User resource returned by the ReviewBoard Web API.
 * Details in:  https://www.reviewboard.org/docs/manual/2.0/webapi/2.0/resources/user/
 */
public class User {

    public String username;
    public String first_name;
    public String last_name;
    public String email;
    public String fullname;
    public Long id;

}
