package com.aurea.prstatus.integration.reviewboard;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ReviewBoardService {

    @GET("review-requests/")
    Call<ReviewRequestResponse> getReviewRequests(@Query("start") Long start, @Query("max-results") Long max);

    @GET("users/")
    Call<UserResponse> getUsers(@Query("start") Long start, @Query("max-results") Long max);

}
