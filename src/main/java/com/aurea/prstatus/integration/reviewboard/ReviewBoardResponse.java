package com.aurea.prstatus.integration.reviewboard;

import com.aurea.prstatus.integration.reviewboard.model.Links;

/**
 * Created by jett on 1/23/16.
 */
public class ReviewBoardResponse {
    Long total_results;
    String stat;Links links;
}
