package com.aurea.prstatus.integration.reviewboard;

import com.aurea.prstatus.integration.ServiceBuilder;
import com.aurea.prstatus.integration.reviewboard.model.ReviewRequest;
import com.aurea.prstatus.integration.reviewboard.model.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import retrofit2.Call;

import java.io.IOException;
import java.util.*;

public class ReviewBoardRepository {

    ReviewBoardService reviewBoardService;

    /**
     * initiates a session with the ReviewBoard API service
     * @param baseUrl - the base URL for ReviewBoard
     * @param userid - userid for ReviewBoard
     * @param password - password for ReviewBoard user
     */
    public ReviewBoardRepository(String baseUrl, String userid, String password) {
        this.reviewBoardService = ServiceBuilder.createService(ReviewBoardService.class, baseUrl, userid, password);
    }

    /**
     * get all pending ReviewBoard review requests
     * @return a list of review requests
     * @throws IOException
     */
    public  List<ReviewRequest> getReviewRequests() throws IOException {

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();
        Boolean isLastPage = false;
        Long start = 0L;
        final Long maxResults = 200L;

        List<ReviewRequest> allReviewRequests = new ArrayList<>();

        while (!isLastPage) {

            Call<ReviewRequestResponse> call = reviewBoardService.getReviewRequests(start, maxResults);
            ReviewRequestResponse response = call.execute().body();

            if ("ok".equals(response.stat)) {
                if (response.review_requests.size() > 0) {

                    Iterator<Map> iter = response.review_requests.iterator();

                    while (iter.hasNext()) {
                        Map repoMap = iter.next();

                        JsonElement jsonElement = gson.toJsonTree(repoMap);
                        ReviewRequest reviewRequest = gson.fromJson(jsonElement, ReviewRequest.class);

                        allReviewRequests.add(reviewRequest);

                    }

                    if (response.links.next == null) {
                        isLastPage = true;
                    } else {
                        start += maxResults;
                    }

                } else {
                    break;
                }
            }
        }

        return allReviewRequests;
    }

    /**
     *
     * @return
     * @throws IOException
     */
    public List<User> getUsers() throws IOException {

        Gson gson = new Gson();
        Boolean isLastPage = false;
        Long start = 0L;
        final Long maxResults = 200L;

        List<User> allUsers = new ArrayList<>();

        while (!isLastPage) {

            Call<UserResponse> call = reviewBoardService.getUsers(start, maxResults);
            UserResponse response = call.execute().body();

            if ("ok".equals(response.stat)) {
                if (response.users.size() > 0) {

                    Iterator<Map> iter = response.users.iterator();

                    while (iter.hasNext()) {
                        Map repoMap = iter.next();

                        JsonElement jsonElement = gson.toJsonTree(repoMap);
                        User user = gson.fromJson(jsonElement, User.class);

                        allUsers.add(user);

                    }

                    if (response.links.next == null) {
                        isLastPage = true;
                    } else {
                        start += maxResults;
                    }

                } else {
                    break;
                }
            }
        }
        return allUsers;
    }


    public static void main(String[] args) {

        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

        ReviewBoardRepository reviewBoardRepository = new ReviewBoardRepository("http://review.aurea.local/api/",
                "review.service",
                "wh38ifkPoU");

        try {
            reviewBoardRepository.getReviewRequests();
//            reviewBoardRepository.getUsers();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
