package com.aurea.prstatus.integration.reviewboard.model;

import java.util.Date;
import java.util.Set;

/**
 * This class represents a partial representation of the ReviewRequest resource returned by the ReviewBoard Web API.
 * Details in:  https://www.reviewboard.org/docs/manual/2.0/webapi/2.0/resources/review-request/
 */
public class ReviewRequest {

    public Long id;
    public String status;

    public Boolean approved;
    public String summary;

    // todo: do we use target_groups in practice?
    public Set<HttpHref> target_people;

    public Links links;

    public Date time_added;
    public Date last_updated;

    public String absolute_url;

//    public Date getTimeAdded() {
//        return convertDate(this.time_added);
//    }

//    public Date getLastUpdateDate() {
//        return convertDate(this.last_updated);
//    }
//
//    private Date convertDate(String tzDate) {
//        SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
//        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
//        try {
//            return sdf.parse(tzDate);
//        } catch(ParseException pe) {
//            return null;
//        }
//
//    }



}
