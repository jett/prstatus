package com.aurea.prstatus;

public class Reviewer implements Comparable<Reviewer> {

    public String name;
    public String email;

    public Reviewer(String name, String email) {
        this.name = name;
        this.email = email;
    }

    @Override
    public int compareTo(Reviewer o) {
        if(this.name == null && o.name == null) {
            return 0;
        }

        return this.name.toUpperCase().compareTo(o.name.toUpperCase());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Reviewer reviewer = (Reviewer) o;

        if (!name.equals(reviewer.name)) return false;
        return !(email != null ? !email.equals(reviewer.email) : reviewer.email != null);

    }

    @Override
    public int hashCode() {
        if(name == null) {
            name = "";
        }

        if(email == null) {
            email = "";
        }

        int result = name.hashCode();
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }
}
